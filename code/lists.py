# Testing lists

squares = [1, 4, 9, 16]
print("squares = " + str(squares))
print("squares[0] = " + str(squares[0]))
sliced03 = squares[0:3]
print("sliced = " + str(sliced03))

concatted = squares + squares[-3:]
print("concatted = " + str(concatted))

concatted[4] = 25
concatted[5] = 36
concatted[6] = 49
print("concatted (modified) = " + str(concatted))

concatted.append(64)
print("concatted (appended) = " + str(concatted))
len1 = len(concatted)
print("concatted length = " + str(len1))

concatted[1:4] = [4916]
print("concatted (reduced) = " + str(concatted))
len2 = len(concatted)
print("concatted length = " + str(len2))

concatted[:] = []
print("concatted (cleared) = " + str(concatted))

