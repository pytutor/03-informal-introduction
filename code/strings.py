# Testing strings.

s1 = "I don't know"
print(s1)

# This does not work r'c:\myfolder\' 
# s2 = r"c:\myfolder\"   # ????
s2 = r"c:\myfolder\code"
print(s2)

s4 = """\
Usage: thingy [OPTIONS]
     -h                        Display this usage message
     -H hostname               Hostname to connect to
"""
print (s4)

s5 = 3 * 'ha ' + ", I'm laughing."
print(s5)


print(s5[3] + s5[4])
print('Len = ' + str(len(s5)))

